package AuthClient;

# subclass Net::SMTP::Server::Client with SMTP AUTH PLAIN support
use base qw(Net::SMTP::Server::Client);
use strict;
use warnings;

use MIME::Base64;
use Net::SMTP::Server::Client;

# Ugly, because Net::SMTP::Server::Client uses AUTOLOAD :/
my %_cmds = (
             AUTH => \&_auth,
             DATA => \&Net::SMTP::Server::Client::_data,
             EXPN => \&Net::SMTP::Server::Client::_noway,
             EHLO => \&_ehlo,
             HELO => \&Net::SMTP::Server::Client::_hello,
             HELP => \&Net::SMTP::Server::Client::_help,
             MAIL => \&Net::SMTP::Server::Client::_mail,
             NOOP => \&Net::SMTP::Server::Client::_noop,
             QUIT => \&Net::SMTP::Server::Client::_quit,
             RCPT => \&Net::SMTP::Server::Client::_receipt,
             RSET => \&Net::SMTP::Server::Client::_reset,
             VRFY => \&Net::SMTP::Server::Client::_noway,
            );

sub new {
  my ($class, $sock, $login, $passwd) = @_;
  my $self = $class->SUPER::new($sock);

  $self->{LOGIN}  = $login  || undef;
  $self->{PASSWD} = $passwd || undef;

  $self->{AUTHSTRING} = encode_base64("\0${login}\0${passwd}", '=')
    if defined $self->{LOGIN};

  bless $self => $class;
  return $self;
}

# Copied verbatim from Net::SMTP::Server::Client,
# due to %_cmds being in that package's lexical scope :/
sub process {
  my $self = shift;
  my($cmd, @args);

  my $sock = $self->{SOCK};

  while (<$sock>) {
    # Clean up.
    chomp;
    s/^\s+//;
    s/\s+$//;
    goto bad unless length($_);

    ($cmd, @args) = split(/\s+/);

    $cmd =~ tr/a-z/A-Z/;

    if (!defined($_cmds{$cmd})) {
    bad:
      $self->_put("500 Learn to type!");
      next;
    }

    return(defined($self->{MSG}) ? 1 : 0) unless
      &{$_cmds{$cmd}}($self, \@args);
  }

  return undef;
}

sub _auth {
  my ($self, $args) = @_;

  $self->_put('535 authorization failed') and return -1
    unless $$args[0] =~ /PLAIN/i;

  $self->_put("535-STRING $args->[1]");

  (decode_base64($self->{AUTHSTRING}) eq decode_base64($args->[1]))
    ? $self->_put('235 ok, go ahead')
      : $self->_put('535 authorization failed');
}

sub _ehlo {
  my $self = shift;
  $self->_put("250-net.smtp.server.smarthost ESMTP");
  $self->_put('250-AUTH PLAIN')
    if defined $self->{LOGIN};
  $self->_put('250 8BITMIME');
}

1;
