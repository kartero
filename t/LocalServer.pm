package LocalServer;

# spawn a fake SMTP server, log connections,
# handle SMTP AUTH when necessary
#
# inspired from WWW::Mechanize testsuite :-)
use strict;
use warnings;

use Carp qw(carp croak);
use FindBin;
use File::Spec;
use File::Temp;
use Net::SMTP;
use POSIX qw(:sys_wait_h);
use Test::More;

sub spawn {
  my ($class, %args) = @_;
  my $self = { %args };
  bless $self => $class;

  local $ENV{TEST_SMTP_VERBOSE};
  $ENV{TEST_SMTP_VERBOSE} = 1 if delete $args{debug};

  $self->{delete} = [];
  my ($fh, $logfile) = File::Temp::tempfile();
  close $fh;
  push @{$self->{delete}}, $logfile;
  $self->{logfile} = $logfile;

  my $login  = $args{login} || '';
  my $passwd = $args{passwd} || '';

  my $server_file = File::Spec->catfile($FindBin::Bin, 'log-server');
  open my $server,
    qq'$^X "$server_file" "$args{port}" "$logfile" "$login" "$passwd" |'
    or die "Could not spawn fake SMTP server $server_file: $!\n";
  my $hostname = <$server>;
  chomp $hostname;
  die "Could not find SMTP server hostname" unless $hostname;

  $self->{fh} = $server;
  $self->{server_hostname} = $hostname;

  diag "Started $server_file listening on port $self->{port}";

  return $self;
}

sub port {
  carp __PACKAGE__ . '::port called without a server'
    unless $_[0]->{port};
  return $_[0]->{port};
}

sub hostname {
  carp __PACKAGE__ . '::hostname called without a server'
    unless $_[0]->{server_hostname};
  return $_[0]->{server_hostname};
}

sub stop {
  if ($_[0]->{fh}) {
    my $smtp = new Net::SMTP $_[0]->hostname, Port => $_[0]->port;
    $smtp->mail('closer');
    $smtp->to('nowhere');
    $smtp->data('quit_server');
    $smtp->quit;
    undef $_[0]->{fh};
  }
}

sub get_log_tail {
  my $self  = shift;
  $self->stop;
  open my $log, '<', $self->{logfile}
    or die "Could not retrieve logfile: $!\n";
  my @last = <$log>;
  return $last[$#last-1];
}

sub DESTROY {
  my $self = shift;
  if ($self->{fh}) {
    close $self->{fh};
    delete $self->{fh};
  }
  for my $file (@{$self->{delete}}) {
    unlink $file or warn "Could not remove tempfile $file: $!\n";
  }
}

1;
