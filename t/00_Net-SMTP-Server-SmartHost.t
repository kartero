# -*- Mode: Perl -*-

# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as
# `perl Net-SMTP-Server-SmartHost.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 7;
BEGIN { use_ok('Net::SMTP::Server::SmartHost') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

use strict;
use warnings;

use Sys::Hostname;

DEFAULT_SMARTHOST: {
  my $smarthost = Net::SMTP::Server::SmartHost->new();
  isa_ok($smarthost, 'Net::SMTP::Server::SmartHost', 'default smarthost');
  is($smarthost->{HOST}, hostname, 'default hostname');
  is($smarthost->{PORT}, 25, 'default port');
}

VERB_SMARTHOST: {
  my $smarthost = Net::SMTP::Server::SmartHost->new('smtp.example.com', 587);
  isa_ok($smarthost, 'Net::SMTP::Server::SmartHost', 'given smarthost');
  is($smarthost->{HOST}, 'smtp.example.com', 'given smtp.example.com');
  is($smarthost->{PORT}, 587, 'given port 587');
}
