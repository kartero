# -*- Mode: Perl -*-

# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl 01_relays.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 4;
BEGIN { use_ok('Net::SMTP::Server::SmartHost') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

use strict;
use warnings;

use lib 't/';
use LocalServer;

my $server = spawn LocalServer port => 12525;
isa_ok($server, 'LocalServer');

my $smarthost = new Net::SMTP::Server::SmartHost($server->hostname,
                                                 $server->port);

my @message = (
               'Perl Tester <tester@net.smtp.server.smarthost>',
               [ 'Bar Fooish <bar@fooish.org>' ],
               'hello world from Net::SMTP::Server::SmartHost!',
              );

ok($smarthost->relay(@message), 'simple relay');
like($server->get_log_tail, qr/hello world/, 'got hello world');
