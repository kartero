package Net::SMTP::Server::SmartHost;

use 5.008;
use strict;
use warnings;

use Carp;
use Net::SMTP;
use Sys::Hostname;

our @ISA = qw();

our $VERSION = '0.02';


# Preloaded methods go here.

sub new {
  my $invocant = shift;
  my $class = ref($invocant) || $invocant;
  my $self = {};

  $self->{HOST} = $_[0] || hostname;
  $self->{PORT} = $_[1] || 25;

  return bless $self => $class;
}

sub auth {
  my $self = shift;
  my ($name, $passwd) = @_;

  $self->{AUTHNAME}   = $name   || undef;
  $self->{AUTHPASSWD} = $passwd || undef;
}

sub relay {
  my $self = shift;
  my ($from, $to, $data) = @_;

  my $smarthost = Net::SMTP->new($self->{HOST}, Port => $self->{PORT})
    or croak "Could not connect to smarthost at "
      . "$self->{HOST}:$self->{PORT}: $!\n";

  $smarthost->auth($self->{AUTHNAME}, $self->{AUTHPASSWD})
    if (defined $self->{AUTHNAME} && defined $self->{AUTHPASSWD});

  $smarthost->mail($from);
  $smarthost->recipient(@$to);
  $smarthost->data( [$data] );

  $smarthost->quit;
}

1;
__END__
# POD follows

=head1 NAME

Net::SMTP::Server::SmartHost - relay mails to a smarhost from a
Net::SMTP::Server


=head1 SYNOPSIS

  use Net::SMTP::Server;
  use Net::SMTP::Server::Client;
  use Net::SMTP::Server::SmartHost;

  # Start an SMTP server using a smarthost
  $server = Net::SMTP::Server->new('localhost', 2525);
  $smtp   = Net::SMTP::Server::SmartHost->new('smtp.remote.com', 587);
  while ($conn = $server->accept) {
    $client = Net::SMTP::Server::Client->new($conn);
    $client->process || next;

    $smtp->relay($client->{FROM}, $client->{TO}, $client->{MSG});
  }


=head1 DESCRIPTION

Net::SMTP::Server::SmartHost is a helper module for Net::SMTP::Server.
Users can create a SMTP::SmartHost instance, configured with the name of
the remost host and port, and let a running SMTP::Server instance relay
mails to the SMTP::SmartHost instance.


=head2 METHODS

=over 4

=item new([HOST, PORT])

Instantiate a new SmartHost instance.  If specified, use the smarthost
at C<HOST:PORT> instead of the the default (C<localhost:25>).

=item relay(FROM, TO, MSG)

Relay a mail (whose contents is determined in FROM, TO, and MSG; see
L<Net::SMTP::Server::Client>) to the smarthost.

=back


=head1 SEE ALSO

L<Net::SMTP>, L<Net::SMTP::Server>, L<Net::SMTP::Server::Client>.

L<Net::SMTP::Server::SmartHost> is part of the L<kartero> suite.


=head1 AUTHOR

Zak B. Elep, E<lt>zakame@spunge.orgE<gt>


=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007 by Zak B. Elep

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut

